import { MutationDeleteCategoryArgs } from "../graphql/mutation/DeleteCategory";
import CategoryService from "../service/Category.service";
import CustomError from "../utils/CustomError";
const DeleteCategory = async (args: MutationDeleteCategoryArgs) => {
  const { id } = args.input;
  const check3 = await CategoryService.DeleteCategory(id);
  if (!check3) {
    throw new CustomError(400, ["ID not exist "]);
  }
  await CategoryService.DeleteCategory(id);
  return { message: "Delete thành công" };
};
export default DeleteCategory;

// đi ra ngoài để lấy về là sử dụng promise
