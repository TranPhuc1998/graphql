import { ApolloServer } from "apollo-server-express";
import { Response } from "express";
import path from "path";
import { buildSchemaSync } from "type-graphql";
// cấu hình graphql
let response: Response;
const Hello = () => {
  // khi goi api  = graphql
  // run context first
  return new ApolloServer({
    uploads: false,
    context: ({ req, res }) => {
      //Req's Graphql
      // khong xu ly o day
      // cấu hình ngôn ngữ (ngôn ngữ măc định )

      response = res;
      return { req, res };
    },
    formatError: (err) => {
      // customError lại cái trả lỗi
        // khi gọi customError api bị lỗi sẽ chạy formatError

      let originalError;
      if (err.originalError) {
        const { status, errors, stack, field }: any = err.originalError;
        originalError = { status, errors, stack, field };
        response.status(originalError?.status); // set status để quăng ra lỗi 4 xị 5 xị tử không tử 
      }
      // những lỗi của graphql
      const resp = {
        status: originalError?.status || 500,
        field: originalError?.field,
        message: err.message,
        errors: originalError?.errors,
      };
      if (resp.status === 500)
        // tslint:disable-next-line:no-console
        console.log("ERROR", { ...resp, stack: originalError?.stack });
      return resp;
    },
    schema: buildSchemaSync({
      // gọi chung cho mutation, query
      // path nhận tất cả các file là ts or js  trong thư mục graphql
      //__dirname trong lib nodejs là đường dẫn tới thư mục hiện tại là lib

      resolvers: [path.join(__dirname, "../graphql/**/*.{ts,js}")],
    }),
  });
};

export default Hello();
