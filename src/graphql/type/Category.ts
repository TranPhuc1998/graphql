import { Field, ObjectType } from "type-graphql";
// import Perfumes from "./Perfume";

@ObjectType()
export class Category {
  @Field()
  id: number;
  @Field()
  name: string;
}

@ObjectType()
export class DeleteCategory {
  @Field()
  message: string;
}
