import {
  ArgsType,
  Field,
  InputType,
  Int,
  Mutation,
  Resolver,
  Args,
} from "type-graphql";

import UpdatePerfumeController from "../../controller/UpdatePerfume.controller";
import UpdatePerfumes from "../../graphql/type/Perfume";

@InputType()
export class UpdatePerfumeInput {
  @Field(() => Int)
  id: number;
  @Field(() => String, { nullable: true })
  name: string;
  @Field(() => Int, { nullable: true })
  categoryId: number;
  @Field(() => [String], { nullable: true }) // mảng thì khai báo kiểu như vậy
  imageUrl: string[];
  @Field(() => Int, { nullable: true })
  price: number;
  @Field(() => String, { nullable: true })
  description: string;
  @Field(() => Int, { nullable: true })
  brandId: number;
  @Field(() => Int, { nullable: true })
  sizeId: number;
}

@ArgsType()
export class MutationUpdatePerfumeArgs {
  @Field(() => UpdatePerfumeInput)
  input: UpdatePerfumeInput; // khai báo 1 type là input là cha có kiểu laà những class con truyền vô dây
}

@Resolver()
export class CreatePerfume {
  @Mutation(() => UpdatePerfumes.CreatePerfumes) // [type]
  async UpdatePerfume(@Args() args: MutationUpdatePerfumeArgs) {
    return await UpdatePerfumeController(args);
    // gọi controller
  }
}
