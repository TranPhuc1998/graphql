import {
  ArgsType,
  Field,
  InputType,
  Int,
  Mutation,
  Resolver,
  Args,
} from "type-graphql";

import DeletePerfumeController from "../../controller/DeletePerfume.controller";
import DeletePerfumeType from "../../graphql/type/Perfume";

@InputType()
export class DeletePerfumeInput {
  @Field(() => Int)
  id: number;
}

@ArgsType()
export class MutationDeletePerfumeArgs {
  @Field(() => DeletePerfumeInput)
  input: DeletePerfumeInput; // khai báo 1 type là input là cha có kiểu laà những class con truyền vô dây
}

@Resolver()
export class DeletePerfume {
  @Mutation(() => DeletePerfumeType.DeletePerfumes) // [type]
  async DeletePerfume(@Args() args: MutationDeletePerfumeArgs) {
    return await DeletePerfumeController(args);
    // gọi controller
  }
}
