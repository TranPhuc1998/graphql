import {
  ArgsType,
  Field,
  InputType,
  Int,
  Mutation,
  Resolver,
  Args,
} from "type-graphql";

import CreatePerfumeController from "../../controller/CreatePerfume.controller";
import CreatePerfumes from "../../graphql/type/Perfume";

@InputType()
export class CreatePerfumeInput {
  @Field(() => String)
  name: string;
  @Field(() => Int)
  categoryId: number;
  @Field(() => [String]) // mảng thì khai báo kiểu như vậy
  imageUrl: string[];
  @Field(() => Int)
  price: number;
  @Field(() => String)
  description: string;
  @Field(() => Int)
  brandId: number;
  @Field(() => Int)
  sizeId: number;
}

@ArgsType()
export class MutationCreatePerfumeArgs {
  @Field(() => CreatePerfumeInput)
  input: CreatePerfumeInput; // khai báo 1 type là input là cha có kiểu laà những class con truyền vô dây
}

@Resolver()
export class CreatePerfume {
  @Mutation(() => CreatePerfumes.CreatePerfumes) // [type]
  async CreatePerfume(@Args() args: MutationCreatePerfumeArgs) {
    return await CreatePerfumeController(args);
    // gọi controller
  }
}

// Cách 1:

// @Resolver()
// export class CreatePerfume {
//   @Mutation(() => CreatePerfumes.CreatePerfumes) // [type]
//   async CreatePerfume(
//     @Arg("name", () => String) name: string,
//     @Arg("imageUrl", () => [String]) imageUrl: string[],
//     @Arg("price", () => Float) price: number,
//     @Arg("description", () => String) description: string,
//     @Arg("categoryId", () => Int) categoryId: number
//   ) {
//     return await CreatePerfumeController(
//       // gọi controller
//       name,
//       imageUrl,
//       price,
//       description,
//       categoryId
//     );
//   }
// }
