import {
  ArgsType,
  Field,
  InputType,
  Int,
  Mutation,
  Resolver,
  Args,
} from "type-graphql";

import UpdateCategoryController from "../../controller/UpdateCategory.controller";
import { Category } from "../type/Category";

@InputType()
export class UpdateCategoryInput {
  @Field(() => Int)
  id: number;
  @Field(() => String, { nullable: true })
  name: string;
}

@ArgsType()
export class MutationUpdateCategoryArgs {
  @Field(() => UpdateCategoryInput)
  input: UpdateCategoryInput; // khai báo 1 type là input là cha có kiểu laà những class con truyền vô dây
}

@Resolver()
export class updateCategory {
  @Mutation(() => Category) // [type]
  async UpdateCategory(@Args() args: MutationUpdateCategoryArgs) {
    // những trường cần thay đổi gọi từ class trên
    return await UpdateCategoryController(args); // return controller
    // gọi controller
  }
}
