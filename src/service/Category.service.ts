import { initModels } from "../models/init-models";
import ConnectDB from "../lib/ConnectPostgre";

//
const db = initModels(ConnectDB);

const GetAll = async () => {
  try {
    const a = await db.Category.findAll({
      // as : tên cột
    });
    return a;
  } catch (err) {
    console.log("loi", err);
    return [];
  }
};
const GetById = async (id: number) => {
  try {
    const a = await db.Category.findOne({
      // include: [{ model: Category, as: "category" }],

      where: {
        id: id,
      },
    });

    return a;
  } catch (err) {
    console.log("loi", err);
    return null;
  }
};

const CreateCategory = async (name: string) => {
  try {
    const a = await db.Category.create({
      // đi ra ngoài  nó data về là promise
      name,
    });

    return a;
  } catch (err) {
    console.log("loi", err);
    return null;
  }
};

const UpdateCategory = async (name: string, id: number) => {
  try {
    const a = await db.Category.update({ name }, { where: { id: id } });
    return a;
  } catch (err) {
    console.log("loi", err);
    return null;
  }
};

const DeleteCategory = async (id: number) => {
  try {
    const a = await db.Category.destroy({ where: { id: id } });
    return a;
  } catch (err) {
    console.log("loi", err);
    return null;
  }
};

export default {
  GetAll,
  CreateCategory,
  UpdateCategory,
  GetById,
  DeleteCategory,
};
// trước hết xún service làm trước, kết nối tới cái bảng mình muốn kết nối database
