import {
  ArgsType,
  Field,
  InputType,
  Int,
  Mutation,
  Resolver,
  Args,
} from "type-graphql";

import DeleteCategoryController from "../../controller/DeleteCategory.controller";
import { DeleteCategory } from "../../graphql/type/Category";

@InputType() // Query Variables nơi input
export class DeleteCategoryInput {
  @Field(() => Int)
  id: number;
}

@ArgsType()
export class MutationDeleteCategoryArgs {
  @Field(() => DeleteCategoryInput)
  input: DeleteCategoryInput; // khai báo 1 type là input là cha có kiểu laà những class con truyền vô dây
}

@Resolver()
export class DestroyCategory {
  @Mutation(() => DeleteCategory) // [type]
  async DeleteCategory(@Args() args: MutationDeleteCategoryArgs) {
    return await DeleteCategoryController(args);
    // gọi controller
  }
}
