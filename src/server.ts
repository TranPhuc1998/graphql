import express from "express";
import "reflect-metadata";
import Graphql from "./lib/Graphql";
// import ConnectDB from "./lib/ConnectPostgre";
// import { initModels } from "./models/init-models";

const app = express();

// 4-8 rest api
// app.get("/perfume", (_req, res) => {
//   return res.json({
//     name: "perfume",
//   });
// });
// express với graphql
Graphql.applyMiddleware({ app, path: "/PhucHong" });
module.exports = app;

export default app;
