import {
  Args,
  ArgsType,
  Field,
  InputType,
  Int,
  Query,
  Resolver,
} from "type-graphql";

import Perfume from "../type/Perfume";
import GetPerfumesQuery from "../../controller/GetPerfumes.controller";

@InputType()
export class GetPerfumeInput {
  @Field(() => Int)
  limit: number;

  @Field(() => Int)
  page: number;
}

@ArgsType()
export class QueryGetPerfumeArgs {
  @Field(() => GetPerfumeInput)
  input: GetPerfumeInput; // khai báo 1 type là input là cha có kiểu laà những class con truyền vô dây
}

@Resolver()
export class GetPerfumes {
  // khai báo function trả về chuỗi string
  // return kiểu khác sẽ báo lỗi
  @Query(() => [Perfume.Perfumes]) // [type]
  async GetPerfumes(@Args() args: QueryGetPerfumeArgs) {
    return await GetPerfumesQuery(args);
  }
}

// Cách 1
// @Resolver()
// export class GetPerfumes {
//   // khai báo function trả về chuỗi string
//   // return kiểu khác sẽ báo lỗi
//   @Query(() => [Perfume.Perfumes]) // [type]
//   async GetPerfumes(
//     @Arg("limit", () => Int) limit: number,
//     @Arg("page", () => Int) page: number
//   ) {
//     return await GetPerfumesQuery(limit, page);
//   }
// }
//Truy Vấn
// const db = initModels(ConnectDB);

// db.Perfume.findAll()
//   .then((success) => {
//     console.log(success);
//   })
//   .catch((err) => {
//     console.log("err ", err);
//   }); // findAll sẽ là selec *

// db.Perfume.findOne({
//   where: {
//     id: 7,
//   },
// })
//   .then((success) => {
//     console.log(success);
//   })
//   .catch((err) => {
//     console.log("err ", err);
//   }); // findAll sẽ là selec *

// db.Perfume.findAll({ order: [["id", "ASC"]] })
//   .then((success) => {
//     console.log(success);
//   })
//   .catch((err) => {
//     console.log("err ", err);
//   }); // findAll sẽ là selec *

// db.Perfume.create({ id: 12, name: "PhatSuper", categoryId: 1 }, {})
//   .then((success) => {
//     console.log(success);
//   })
//   .catch((err) => {
//     console.log("err ", err);
