import {
  Args,
  ArgsType,
  Field,
  InputType,
  Mutation,
  Resolver,
} from "type-graphql";

import CreateCategoryController from "../../controller/CreateCategory.controller";
import { Category } from "../../graphql/type/Category";

@InputType()
export class CreateCategoryInput {
  @Field(() => String)
  name: string;
}

@ArgsType()
export class MutationCreateCategoryArgs {
  @Field(() => CreateCategoryInput)
  input: CreateCategoryInput; // khai báo 1 type là input là cha có kiểu laà những class con truyền vô dây
}

@Resolver() // khai bái nutation or query
export class CreateCategory {
  @Mutation(() => Category) // [type]
  async CreateCategory(@Args() args: MutationCreateCategoryArgs) {
    return await CreateCategoryController(args);
  }
}
