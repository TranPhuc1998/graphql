import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';
import type { Order, OrderId } from './Order';
import type { Perfume, PerfumeId } from './Perfume';

export interface OrderDetailAttributes {
  orderId: number;
  perfumeId: number;
  price?: number;
  quantity?: number;
}

export type OrderDetailPk = "orderId" | "perfumeId";
export type OrderDetailId = OrderDetail[OrderDetailPk];
export type OrderDetailCreationAttributes = Optional<OrderDetailAttributes, OrderDetailPk>;

export class OrderDetail extends Model<OrderDetailAttributes, OrderDetailCreationAttributes> implements OrderDetailAttributes {
  orderId!: number;
  perfumeId!: number;
  price?: number;
  quantity?: number;

  // OrderDetail belongsTo Order via orderId
  order!: Order;
  getOrder!: Sequelize.BelongsToGetAssociationMixin<Order>;
  setOrder!: Sequelize.BelongsToSetAssociationMixin<Order, OrderId>;
  createOrder!: Sequelize.BelongsToCreateAssociationMixin<Order>;
  // OrderDetail belongsTo Perfume via perfumeId
  perfume!: Perfume;
  getPerfume!: Sequelize.BelongsToGetAssociationMixin<Perfume>;
  setPerfume!: Sequelize.BelongsToSetAssociationMixin<Perfume, PerfumeId>;
  createPerfume!: Sequelize.BelongsToCreateAssociationMixin<Perfume>;

  static initModel(sequelize: Sequelize.Sequelize): typeof OrderDetail {
    OrderDetail.init({
    orderId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'Order',
        key: 'id'
      },
      field: 'orderID'
    },
    perfumeId: {
      type: DataTypes.SMALLINT,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'Perfume',
        key: 'id'
      },
      field: 'perfumeID'
    },
    price: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    quantity: {
      type: DataTypes.SMALLINT,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'OrderDetail',
    schema: 'product',
    timestamps: false,
    indexes: [
      {
        name: "OrderDetail_pkey",
        unique: true,
        fields: [
          { name: "orderID" },
          { name: "perfumeID" },
        ]
      },
    ]
  });
  return OrderDetail;
  }
}
