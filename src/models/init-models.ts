import type { Sequelize } from "sequelize";
import { Brand } from "./Brand";
import type { BrandAttributes, BrandCreationAttributes } from "./Brand";
import { Category } from "./Category";
import type {
  CategoryAttributes,
  CategoryCreationAttributes,
} from "./Category";
import { Order } from "./Order";
import type { OrderAttributes, OrderCreationAttributes } from "./Order";
import { OrderDetail } from "./OrderDetail";
import type {
  OrderDetailAttributes,
  OrderDetailCreationAttributes,
} from "./OrderDetail";
import { Perfume } from "./Perfume";
import type { PerfumeAttributes, PerfumeCreationAttributes } from "./Perfume";
import { Person } from "./Person";
import type { PersonAttributes, PersonCreationAttributes } from "./Person";
import { Size } from "./Size";
import type { SizeAttributes, SizeCreationAttributes } from "./Size";

export { Brand, Category, Order, OrderDetail, Perfume, Person, Size };

export type {
  BrandAttributes,
  BrandCreationAttributes,
  CategoryAttributes,
  CategoryCreationAttributes,
  OrderAttributes,
  OrderCreationAttributes,
  OrderDetailAttributes,
  OrderDetailCreationAttributes,
  PerfumeAttributes,
  PerfumeCreationAttributes,
  PersonAttributes,
  PersonCreationAttributes,
  SizeAttributes,
  SizeCreationAttributes,
};

export function initModels(sequelize: Sequelize) {
  Brand.initModel(sequelize);
  Category.initModel(sequelize);
  Order.initModel(sequelize);
  OrderDetail.initModel(sequelize);
  Perfume.initModel(sequelize);
  Person.initModel(sequelize);
  Size.initModel(sequelize);

  Order.belongsToMany(Perfume, {
    as: "perfumeIdPerfumes",
    through: OrderDetail,
    foreignKey: "orderId",
    otherKey: "perfumeId",
  });
  Perfume.belongsToMany(Order, {
    as: "orderIdOrders",
    through: OrderDetail,
    foreignKey: "perfumeId",
    otherKey: "orderId",
  });
  Perfume.belongsTo(Brand, { as: "brand", foreignKey: "brandId" });
  Brand.hasMany(Perfume, { as: "perfumes", foreignKey: "brandId" });
  Perfume.belongsTo(Category, { as: "category", foreignKey: "categoryId" });
  Category.hasMany(Perfume, { as: "perfumes", foreignKey: "categoryId" });
  OrderDetail.belongsTo(Order, { as: "order", foreignKey: "orderId" });
  Order.hasMany(OrderDetail, { as: "orderDetails", foreignKey: "orderId" });
  OrderDetail.belongsTo(Perfume, { as: "perfume", foreignKey: "perfumeId" });
  Perfume.hasMany(OrderDetail, { as: "orderDetails", foreignKey: "perfumeId" });
  Order.belongsTo(Person, { as: "createByPerson", foreignKey: "createBy" });
  Person.hasMany(Order, { as: "orders", foreignKey: "createBy" });
  Perfume.belongsTo(Size, { as: "size", foreignKey: "sizeId" });
  Size.hasMany(Perfume, { as: "perfumes", foreignKey: "sizeId" });

  return {
    Brand: Brand,
    Category: Category,
    Order: Order,
    OrderDetail: OrderDetail,
    Perfume: Perfume,
    Person: Person,
    Size: Size,
  };
}
