export default class CustomError extends Error {
  status: number;
  errors: string[];

  constructor(status: number, errors: string[]) {
    super(); //  những thuộc tính có sẵn của thằng error đó
    this.errors = errors;
    this.status = status;
  }
}
