import { Query, Resolver } from "type-graphql";

import { Category } from "../type/Category";
import GetCategoryQuery from "../../controller/GetCategory.controller";
@Resolver()
export class GetCategory {
  @Query(() => [Category]) // [type]
  async GetCategory() {
    return await GetCategoryQuery();
  }
}
