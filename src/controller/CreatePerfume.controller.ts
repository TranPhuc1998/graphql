import { MutationCreatePerfumeArgs } from "../graphql/mutation/CreatePerfume";
import PerfumeService from "../service/Perfume.service";
const CreatePerfume = async (args: MutationCreatePerfumeArgs) => {
  const { name, categoryId, description, imageUrl, price, brandId, sizeId } =
    args.input;
  const check = await PerfumeService.Create(
    name,
    imageUrl,
    price,
    description,
    categoryId,
    brandId,
    sizeId
  );
  if (check === null) {
    throw new Error("lỗi");
  }
  return check;
};
export default CreatePerfume;

// đúng theo thứ tự ở service
