import { Field, ObjectType } from "type-graphql";

@ObjectType()
export class Brand {
  @Field()
  id: number;
  @Field()
  name: string;
  @Field()
  image: string;
}
