import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';
import type { OrderDetail, OrderDetailId } from './OrderDetail';
import type { Perfume, PerfumeId } from './Perfume';
import type { Person, PersonId } from './Person';

export interface OrderAttributes {
  id?: number;
  createdAt?: Date;
  createBy: number;
  totalMoney: number;
}

export type OrderPk = "id";
export type OrderId = Order[OrderPk];
export type OrderCreationAttributes = Optional<OrderAttributes, OrderPk>;

export class Order extends Model<OrderAttributes, OrderCreationAttributes> implements OrderAttributes {
  id?: number;
  createdAt?: Date;
  createBy!: number;
  totalMoney!: number;

  // Order hasMany OrderDetail via orderId
  orderDetails!: OrderDetail[];
  getOrderDetails!: Sequelize.HasManyGetAssociationsMixin<OrderDetail>;
  setOrderDetails!: Sequelize.HasManySetAssociationsMixin<OrderDetail, OrderDetailId>;
  addOrderDetail!: Sequelize.HasManyAddAssociationMixin<OrderDetail, OrderDetailId>;
  addOrderDetails!: Sequelize.HasManyAddAssociationsMixin<OrderDetail, OrderDetailId>;
  createOrderDetail!: Sequelize.HasManyCreateAssociationMixin<OrderDetail>;
  removeOrderDetail!: Sequelize.HasManyRemoveAssociationMixin<OrderDetail, OrderDetailId>;
  removeOrderDetails!: Sequelize.HasManyRemoveAssociationsMixin<OrderDetail, OrderDetailId>;
  hasOrderDetail!: Sequelize.HasManyHasAssociationMixin<OrderDetail, OrderDetailId>;
  hasOrderDetails!: Sequelize.HasManyHasAssociationsMixin<OrderDetail, OrderDetailId>;
  countOrderDetails!: Sequelize.HasManyCountAssociationsMixin;
  // Order belongsToMany Perfume via orderId and perfumeId
  perfumeIdPerfumes!: Perfume[];
  getPerfumeIdPerfumes!: Sequelize.BelongsToManyGetAssociationsMixin<Perfume>;
  setPerfumeIdPerfumes!: Sequelize.BelongsToManySetAssociationsMixin<Perfume, PerfumeId>;
  addPerfumeIdPerfume!: Sequelize.BelongsToManyAddAssociationMixin<Perfume, PerfumeId>;
  addPerfumeIdPerfumes!: Sequelize.BelongsToManyAddAssociationsMixin<Perfume, PerfumeId>;
  createPerfumeIdPerfume!: Sequelize.BelongsToManyCreateAssociationMixin<Perfume>;
  removePerfumeIdPerfume!: Sequelize.BelongsToManyRemoveAssociationMixin<Perfume, PerfumeId>;
  removePerfumeIdPerfumes!: Sequelize.BelongsToManyRemoveAssociationsMixin<Perfume, PerfumeId>;
  hasPerfumeIdPerfume!: Sequelize.BelongsToManyHasAssociationMixin<Perfume, PerfumeId>;
  hasPerfumeIdPerfumes!: Sequelize.BelongsToManyHasAssociationsMixin<Perfume, PerfumeId>;
  countPerfumeIdPerfumes!: Sequelize.BelongsToManyCountAssociationsMixin;
  // Order belongsTo Person via createBy
  createByPerson!: Person;
  getCreateByPerson!: Sequelize.BelongsToGetAssociationMixin<Person>;
  setCreateByPerson!: Sequelize.BelongsToSetAssociationMixin<Person, PersonId>;
  createCreateByPerson!: Sequelize.BelongsToCreateAssociationMixin<Person>;

  static initModel(sequelize: Sequelize.Sequelize): typeof Order {
    Order.init({
    id: {
      autoIncrement: true,
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true
    },
    createBy: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'Person',
        key: 'Person_ID'
      }
    },
    totalMoney: {
      type: DataTypes.DECIMAL,
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'Order',
    schema: 'product',
    timestamps: true,
    indexes: [
      {
        name: "Order_pkey",
        unique: true,
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
  return Order;
  }
}
