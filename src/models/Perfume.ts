import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';
import type { Brand, BrandId } from './Brand';
import type { Category, CategoryId } from './Category';
import type { Order, OrderId } from './Order';
import type { OrderDetail, OrderDetailId } from './OrderDetail';
import type { Size, SizeId } from './Size';

export interface PerfumeAttributes {
  id?: number;
  name?: string;
  categoryId?: number;
  imageUrl?: string[];
  price?: number;
  description?: string;
  brandId: number;
  sizeId: number;
}

export type PerfumePk = "id";
export type PerfumeId = Perfume[PerfumePk];
export type PerfumeCreationAttributes = Optional<PerfumeAttributes, PerfumePk>;

export class Perfume extends Model<PerfumeAttributes, PerfumeCreationAttributes> implements PerfumeAttributes {
  id?: number;
  name?: string;
  categoryId?: number;
  imageUrl?: string[];
  price?: number;
  description?: string;
  brandId!: number;
  sizeId!: number;

  // Perfume belongsTo Brand via brandId
  brand!: Brand;
  getBrand!: Sequelize.BelongsToGetAssociationMixin<Brand>;
  setBrand!: Sequelize.BelongsToSetAssociationMixin<Brand, BrandId>;
  createBrand!: Sequelize.BelongsToCreateAssociationMixin<Brand>;
  // Perfume belongsTo Category via categoryId
  category!: Category;
  getCategory!: Sequelize.BelongsToGetAssociationMixin<Category>;
  setCategory!: Sequelize.BelongsToSetAssociationMixin<Category, CategoryId>;
  createCategory!: Sequelize.BelongsToCreateAssociationMixin<Category>;
  // Perfume belongsToMany Order via perfumeId and orderId
  orderIdOrders!: Order[];
  getOrderIdOrders!: Sequelize.BelongsToManyGetAssociationsMixin<Order>;
  setOrderIdOrders!: Sequelize.BelongsToManySetAssociationsMixin<Order, OrderId>;
  addOrderIdOrder!: Sequelize.BelongsToManyAddAssociationMixin<Order, OrderId>;
  addOrderIdOrders!: Sequelize.BelongsToManyAddAssociationsMixin<Order, OrderId>;
  createOrderIdOrder!: Sequelize.BelongsToManyCreateAssociationMixin<Order>;
  removeOrderIdOrder!: Sequelize.BelongsToManyRemoveAssociationMixin<Order, OrderId>;
  removeOrderIdOrders!: Sequelize.BelongsToManyRemoveAssociationsMixin<Order, OrderId>;
  hasOrderIdOrder!: Sequelize.BelongsToManyHasAssociationMixin<Order, OrderId>;
  hasOrderIdOrders!: Sequelize.BelongsToManyHasAssociationsMixin<Order, OrderId>;
  countOrderIdOrders!: Sequelize.BelongsToManyCountAssociationsMixin;
  // Perfume hasMany OrderDetail via perfumeId
  orderDetails!: OrderDetail[];
  getOrderDetails!: Sequelize.HasManyGetAssociationsMixin<OrderDetail>;
  setOrderDetails!: Sequelize.HasManySetAssociationsMixin<OrderDetail, OrderDetailId>;
  addOrderDetail!: Sequelize.HasManyAddAssociationMixin<OrderDetail, OrderDetailId>;
  addOrderDetails!: Sequelize.HasManyAddAssociationsMixin<OrderDetail, OrderDetailId>;
  createOrderDetail!: Sequelize.HasManyCreateAssociationMixin<OrderDetail>;
  removeOrderDetail!: Sequelize.HasManyRemoveAssociationMixin<OrderDetail, OrderDetailId>;
  removeOrderDetails!: Sequelize.HasManyRemoveAssociationsMixin<OrderDetail, OrderDetailId>;
  hasOrderDetail!: Sequelize.HasManyHasAssociationMixin<OrderDetail, OrderDetailId>;
  hasOrderDetails!: Sequelize.HasManyHasAssociationsMixin<OrderDetail, OrderDetailId>;
  countOrderDetails!: Sequelize.HasManyCountAssociationsMixin;
  // Perfume belongsTo Size via sizeId
  size!: Size;
  getSize!: Sequelize.BelongsToGetAssociationMixin<Size>;
  setSize!: Sequelize.BelongsToSetAssociationMixin<Size, SizeId>;
  createSize!: Sequelize.BelongsToCreateAssociationMixin<Size>;

  static initModel(sequelize: Sequelize.Sequelize): typeof Perfume {
    Perfume.init({
    id: {
      autoIncrement: true,
      type: DataTypes.SMALLINT,
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    categoryId: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'Category',
        key: 'id'
      },
      field: 'category_id'
    },
    imageUrl: {
      type: DataTypes.ARRAY(DataTypes.TEXT),
      allowNull: true,
      field: 'image_url'
    },
    price: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    brandId: {
      type: DataTypes.SMALLINT,
      allowNull: false,
      references: {
        model: 'Brand',
        key: 'id'
      },
      field: 'brand_id'
    },
    sizeId: {
      type: DataTypes.SMALLINT,
      allowNull: false,
      references: {
        model: 'Size',
        key: 'id'
      },
      field: 'size_id'
    }
  }, {
    sequelize,
    tableName: 'Perfume',
    schema: 'product',
    timestamps: false,
    indexes: [
      {
        name: "Perfume_pkey",
        unique: true,
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
  return Perfume;
  }
}
