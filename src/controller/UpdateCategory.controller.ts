import { MutationUpdateCategoryArgs } from "../graphql/mutation/UpdateCategory";
import CategoryService from "../service/Category.service";
import CustomError from "../utils/CustomError";

const UpdateCategory = async (args: MutationUpdateCategoryArgs) => {
  const { name, id } = args.input;
  const check3 = await CategoryService.GetById(id);
  if (!check3) {
    throw new CustomError(400, ["ID not exist "]);
  }
  // nếu không có quăn lỗi có nó xún dưới
  await CategoryService.UpdateCategory(name, id);
  const check2 = await CategoryService.GetById(id);
  return check2;
};
export default UpdateCategory;
