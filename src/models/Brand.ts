import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';
import type { Perfume, PerfumeId } from './Perfume';

export interface BrandAttributes {
  id?: number;
  name?: string;
  image?: string;
}

export type BrandPk = "id";
export type BrandId = Brand[BrandPk];
export type BrandCreationAttributes = Optional<BrandAttributes, BrandPk>;

export class Brand extends Model<BrandAttributes, BrandCreationAttributes> implements BrandAttributes {
  id?: number;
  name?: string;
  image?: string;

  // Brand hasMany Perfume via brandId
  perfumes!: Perfume[];
  getPerfumes!: Sequelize.HasManyGetAssociationsMixin<Perfume>;
  setPerfumes!: Sequelize.HasManySetAssociationsMixin<Perfume, PerfumeId>;
  addPerfume!: Sequelize.HasManyAddAssociationMixin<Perfume, PerfumeId>;
  addPerfumes!: Sequelize.HasManyAddAssociationsMixin<Perfume, PerfumeId>;
  createPerfume!: Sequelize.HasManyCreateAssociationMixin<Perfume>;
  removePerfume!: Sequelize.HasManyRemoveAssociationMixin<Perfume, PerfumeId>;
  removePerfumes!: Sequelize.HasManyRemoveAssociationsMixin<Perfume, PerfumeId>;
  hasPerfume!: Sequelize.HasManyHasAssociationMixin<Perfume, PerfumeId>;
  hasPerfumes!: Sequelize.HasManyHasAssociationsMixin<Perfume, PerfumeId>;
  countPerfumes!: Sequelize.HasManyCountAssociationsMixin;

  static initModel(sequelize: Sequelize.Sequelize): typeof Brand {
    Brand.init({
    id: {
      autoIncrement: true,
      type: DataTypes.SMALLINT,
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    image: {
      type: DataTypes.STRING(255),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'Brand',
    schema: 'product',
    timestamps: false,
    indexes: [
      {
        name: "Brand_pkey",
        unique: true,
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
  return Brand;
  }
}
