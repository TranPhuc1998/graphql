import { MutationDeletePerfumeArgs } from "../graphql/mutation/DeletePerfume";
import PerfumeService from "../service/Perfume.service";
import CustomError from "../utils/CustomError";
const DeletePerfume = async (args: MutationDeletePerfumeArgs) => {
  const { id } = args.input;
  const check3 = await PerfumeService.DestroyById(id);
  if (!check3) {
    throw new CustomError(400, ["ID not exist "]);
  }
  await PerfumeService.DestroyById(id);
  return { message: "Hello123456" };
};
export default DeletePerfume;
