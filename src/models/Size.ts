import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';
import type { Perfume, PerfumeId } from './Perfume';

export interface SizeAttributes {
  id?: number;
  name?: string;
}

export type SizePk = "id";
export type SizeId = Size[SizePk];
export type SizeCreationAttributes = Optional<SizeAttributes, SizePk>;

export class Size extends Model<SizeAttributes, SizeCreationAttributes> implements SizeAttributes {
  id?: number;
  name?: string;

  // Size hasMany Perfume via sizeId
  perfumes!: Perfume[];
  getPerfumes!: Sequelize.HasManyGetAssociationsMixin<Perfume>;
  setPerfumes!: Sequelize.HasManySetAssociationsMixin<Perfume, PerfumeId>;
  addPerfume!: Sequelize.HasManyAddAssociationMixin<Perfume, PerfumeId>;
  addPerfumes!: Sequelize.HasManyAddAssociationsMixin<Perfume, PerfumeId>;
  createPerfume!: Sequelize.HasManyCreateAssociationMixin<Perfume>;
  removePerfume!: Sequelize.HasManyRemoveAssociationMixin<Perfume, PerfumeId>;
  removePerfumes!: Sequelize.HasManyRemoveAssociationsMixin<Perfume, PerfumeId>;
  hasPerfume!: Sequelize.HasManyHasAssociationMixin<Perfume, PerfumeId>;
  hasPerfumes!: Sequelize.HasManyHasAssociationsMixin<Perfume, PerfumeId>;
  countPerfumes!: Sequelize.HasManyCountAssociationsMixin;

  static initModel(sequelize: Sequelize.Sequelize): typeof Size {
    Size.init({
    id: {
      autoIncrement: true,
      type: DataTypes.SMALLINT,
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'Size',
    schema: 'product',
    timestamps: false,
    indexes: [
      {
        name: "Size_pkey",
        unique: true,
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
  return Size;
  }
}
