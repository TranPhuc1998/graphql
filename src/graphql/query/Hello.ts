import { Query, Resolver } from "type-graphql";

@Resolver()
export class Hello {
  // khai báo function trả về chuỗi string
  // return kiểu khác sẽ báo lỗi
  @Query(() => String)
  HelloPerfume() {
    return "Hello Sensi";
  }
}
