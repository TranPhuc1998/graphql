import { Field, ObjectType } from "type-graphql";
import { Brand } from "./Brand";

import { Category } from "./Category";
import { Size } from "./Size";

@ObjectType()
class Perfumes {
  @Field()
  id: number;
  @Field()
  name: string;
  @Field()
  category: Category;
  @Field(() => [String], { nullable: true }) // mảng thì khai báo kiểu như vậy
  imageUrl: string[];

  @Field({ nullable: true })
  price: number;
  @Field({ nullable: true })
  description: string;
  @Field()
  brand: Brand;
  @Field()
  size: Size;
}

@ObjectType()
class CreatePerfumes {
  @Field()
  id: number;
  @Field()
  name: string;
  @Field()
  categoryId: number;
  @Field(() => [String], { nullable: true }) // mảng thì khai báo kiểu như vậy
  imageUrl: string[];

  @Field({ nullable: true })
  price: number;
  @Field({ nullable: true })
  description: string;
  @Field()
  brandId: number;
  @Field()
  sizeId: number;
}

@ObjectType()
class DeletePerfumes {
  @Field()
  message: string;
}

export default { Perfumes, CreatePerfumes, DeletePerfumes };
