import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';
import type { Perfume, PerfumeId } from './Perfume';

export interface CategoryAttributes {
  id?: number;
  name?: string;
}

export type CategoryPk = "id";
export type CategoryId = Category[CategoryPk];
export type CategoryCreationAttributes = Optional<CategoryAttributes, CategoryPk>;

export class Category extends Model<CategoryAttributes, CategoryCreationAttributes> implements CategoryAttributes {
  id?: number;
  name?: string;

  // Category hasMany Perfume via categoryId
  perfumes!: Perfume[];
  getPerfumes!: Sequelize.HasManyGetAssociationsMixin<Perfume>;
  setPerfumes!: Sequelize.HasManySetAssociationsMixin<Perfume, PerfumeId>;
  addPerfume!: Sequelize.HasManyAddAssociationMixin<Perfume, PerfumeId>;
  addPerfumes!: Sequelize.HasManyAddAssociationsMixin<Perfume, PerfumeId>;
  createPerfume!: Sequelize.HasManyCreateAssociationMixin<Perfume>;
  removePerfume!: Sequelize.HasManyRemoveAssociationMixin<Perfume, PerfumeId>;
  removePerfumes!: Sequelize.HasManyRemoveAssociationsMixin<Perfume, PerfumeId>;
  hasPerfume!: Sequelize.HasManyHasAssociationMixin<Perfume, PerfumeId>;
  hasPerfumes!: Sequelize.HasManyHasAssociationsMixin<Perfume, PerfumeId>;
  countPerfumes!: Sequelize.HasManyCountAssociationsMixin;

  static initModel(sequelize: Sequelize.Sequelize): typeof Category {
    Category.init({
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'Category',
    schema: 'product',
    timestamps: false,
    indexes: [
      {
        name: "Category_pkey",
        unique: true,
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
  return Category;
  }
}
