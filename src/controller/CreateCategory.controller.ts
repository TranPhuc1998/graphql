import { MutationCreateCategoryArgs } from "../graphql/mutation/CreateCategory";
import CategoryService from "../service/Category.service";

const CreateCategory = async (args: MutationCreateCategoryArgs) => {
  const { name } = args.input; //  destructering
  const check = await CategoryService.CreateCategory(name);
  if (check === null) {
    throw new Error("lỗi");
  }
  return check;
};

export default CreateCategory;
