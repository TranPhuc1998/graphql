import { MutationUpdatePerfumeArgs } from "../graphql/mutation/UpdatePerfume";
import PerfumeService from "../service/Perfume.service";
import CustomError from "../utils/CustomError";
const UpdatePerfume = async (args: MutationUpdatePerfumeArgs) => {
  const {
    name,
    categoryId,
    description,
    imageUrl,
    price,
    id,
    brandId,
    sizeId,
  } = args.input;
  const check3 = await PerfumeService.GetById(id);
  if (!check3) {
    throw new CustomError(400, ["ID not exist "]);
  }
  await PerfumeService.Update(
    name,
    imageUrl,
    price,
    description,
    categoryId,
    id,
    brandId,
    sizeId
  );

  const check2 = await PerfumeService.GetById(id);
  return check2;
};
export default UpdatePerfume;
