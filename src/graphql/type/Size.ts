import { Field, ObjectType } from "type-graphql";

@ObjectType()
export class Size {
  @Field()
  id: number;
  @Field()
  name: string;
}
