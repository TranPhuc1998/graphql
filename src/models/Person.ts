import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';
import type { Order, OrderId } from './Order';

export interface PersonAttributes {
  personId?: number;
  personName?: string;
}

export type PersonPk = "personId";
export type PersonId = Person[PersonPk];
export type PersonCreationAttributes = Optional<PersonAttributes, PersonPk>;

export class Person extends Model<PersonAttributes, PersonCreationAttributes> implements PersonAttributes {
  personId?: number;
  personName?: string;

  // Person hasMany Order via createBy
  orders!: Order[];
  getOrders!: Sequelize.HasManyGetAssociationsMixin<Order>;
  setOrders!: Sequelize.HasManySetAssociationsMixin<Order, OrderId>;
  addOrder!: Sequelize.HasManyAddAssociationMixin<Order, OrderId>;
  addOrders!: Sequelize.HasManyAddAssociationsMixin<Order, OrderId>;
  createOrder!: Sequelize.HasManyCreateAssociationMixin<Order>;
  removeOrder!: Sequelize.HasManyRemoveAssociationMixin<Order, OrderId>;
  removeOrders!: Sequelize.HasManyRemoveAssociationsMixin<Order, OrderId>;
  hasOrder!: Sequelize.HasManyHasAssociationMixin<Order, OrderId>;
  hasOrders!: Sequelize.HasManyHasAssociationsMixin<Order, OrderId>;
  countOrders!: Sequelize.HasManyCountAssociationsMixin;

  static initModel(sequelize: Sequelize.Sequelize): typeof Person {
    Person.init({
    personId: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      field: 'Person_ID'
    },
    personName: {
      type: DataTypes.STRING(255),
      allowNull: true,
      field: 'Person_Name'
    }
  }, {
    sequelize,
    tableName: 'Person',
    schema: 'product',
    timestamps: false,
    indexes: [
      {
        name: "Person_pkey",
        unique: true,
        fields: [
          { name: "Person_ID" },
        ]
      },
    ]
  });
  return Person;
  }
}
