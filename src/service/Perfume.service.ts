import { Brand, Category, initModels, Size } from "../models/init-models";
import ConnectDB from "../lib/ConnectPostgre";
// sau này file này sẽ là file view
const db = initModels(ConnectDB);

// sau này sẽ là file Controller
// ... View sẽ gọi Controller

const GetAll = async (limit: number, page: number) => {
  try {
    const a = await db.Perfume.findAll({
      include: [
        { model: Category, as: "category" },
        { model: Brand, as: "brand" },
        { model: Size, as: "size" },
      ],
      limit: limit,
      offset: limit * (page - 1),
    });
    console.log(a[0].category, "a");
    return a;
  } catch (err) {
    console.log("loi", err);
    return [];
  }
};

const GetById = async (id: number) => {
  try {
    const a = await db.Perfume.findOne({
      // include: [{ model: Category, as: "category" }],

      where: {
        id: id,
      },
    });
    console.log(a?.name);
    return a;
  } catch (err) {
    console.log("loi", err);
    return null;
  }
};

const Create = async (
  name: string,
  imageUrl: string[],
  price: number,
  description: string,
  categoryId: number,
  brandId: number,
  sizeId: number
) => {
  try {
    const a = await db.Perfume.create({
      name,
      imageUrl,
      price,
      description,
      categoryId,
      brandId,
      sizeId,
    });

    return a;
  } catch (err) {
    console.log("loi", err);
    return null;
  }
};

const Update = async (
  name: string,
  imageUrl: string[],
  price: number,
  description: string,
  categoryId: number,
  id: number,
  brandId: number,
  sizeId: number
) => {
  try {
    const a = await db.Perfume.update(
      {
        name,
        imageUrl,
        price,
        description,
        categoryId,
        brandId,
        sizeId,
      },
      {
        where: {
          id: id,
        },
      }
    );

    return a;
  } catch (err) {
    console.log("loi", err);
    return null;
  }
};

const DestroyById = async (id: number) => {
  try {
    const a = await db.Perfume.destroy({
      // include: [{ model: Category, as: "category" }],

      where: {
        id: id,
      },
    });
    return a;
  } catch (err) {
    console.log("loi", err);
    return null;
  }
};
export default { GetAll, Create, Update, GetById, DestroyById };

// limit: 2
// page: 1
//  lấy tối đa 2 thằng không bỏ qua thằng nào

// limit : 2
// page :2
