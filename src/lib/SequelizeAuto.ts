import sequelizeAuto from "sequelize-auto";

const SequelizeAutoo = new sequelizeAuto("Perfume", "postgres", "12345678", {
  host: "localhost",
  dialect: "postgres" /* one of 'mysql' | 'mariadb' | 'postgres' | 'mssql' */,
  directory: "./src/models",
  singularize: true,
  caseModel: "p", // tên cuả bảng
  caseProp: "c", //column của bảng
  caseFile: "p", // tên file
  lang: "ts", // tạo file ts
});
SequelizeAutoo.run();
