import CategoryService from "../service/Category.service";
const GetCategory = async () => {
  return await CategoryService.GetAll();
};
export default GetCategory;
// phân trang async (truyền nó vào 2 biến);
