import { QueryGetPerfumeArgs } from "../graphql/query/GetPerfumes";
import PerfumeService from "../service/Perfume.service";

const GetPerfumes = async (args: QueryGetPerfumeArgs) => {
  const { limit, page } = args.input;
  return await PerfumeService.GetAll(limit, page);
};
export default GetPerfumes;
